//��������� ����������� ���������� � �������������� ������������� ��������� 
//TODO : ������� � ������
//TODO : ���������� �� ���
//TODO : �������� ������ ������ � �������
//TODO : ��������� �������� ������������� ����� (�� ��������)
//TODO : �������� �������, ������������ �������
//TODO : ���� ��������� � ������ �� �� �����

#include "stdafx.h"
#include <iostream>
#include <conio.h>
#include <fstream>
#include <string>
#include <vector>
#include <exception>
#include <time.h>
#include <algorithm>

#define max_name 256;
#define inp_err -1
#define save_file_err -2
//#define ESC 27
//#define max_len_str 60
//#define max_len_int_num 5
//#define max_len_disc_num_sem 1
//#define max_len_group_speciality 8
//#define max_pop_num 10

//�������� ����������
//void cat_exc(exception err_msg);
//�������� ������������ ������
//void cat_err();
//���� ������ � ����� �������
void Input_Classes();
//���� ���������� �� ����������
void Input_Classrooms();
//����� ������� ���������� ��������� (���� �����)
//void FAQ_Output();
//������ �� ����� ���������� � ��������
//vector <Classes> Load_Classes();
//�������� � ����� ����������, ��� ������ � ����
//void Genetic_Algorithm(vector <Classes> classes_list);
//������� ������� ���� ��� �����/������ ����������
void Main_Menu();

using namespace std;

//�������������� ���� �������
struct Classes {
	string discipline;
	string teacher;
	string group;
	int time;
	int classroom;
};

//���� ���������
struct Classrooms {
	string number;
	string name;
};

//������� ���� ������
/*struct stud_day {
	int times_number = { 0 }, group_number = { 0 };
	vector <string> days = { "�����������", "�������", "�����", "�������", "�������" };
	char discipline_name[60], teacher_name[60];
}; */

//������ ��� ������ ������
struct program_errors {
	string save_file_disciplines = "�������� ������ ��� ������ � ���� ���������� �� �����������";
	string save_file_teachers = "�������� ������ ��� ������ � ���� ���������� �� ��������������";
	string save_file_groups = "�������� ������ ��� ������ � ���� ���������� �� �������";
	string save_file_classrooms = "�������� ������ ��� ������ � ���� ���������� �� ����������";
	string input_disciplines = "�������� ������ ��� ����� ���������� �� �����������";
	string input_teachers = "�������� ������ ��� ����� ���������� �� ��������������";
	string input_groups = "�������� ������ ��� ����� ���������� �� �������";
	string input_classrooms = "�������� ������ ��� ����� ���������� �� ����������";
	string load_file_disciplines = "�������� ������ ��� ������ �� ����� ���������� �� �����������";
	string load_file_teachers = "�������� ������ ��� ������ �� ����� ���������� �� ��������������";
	string load_file_groups = "�������� ������ ��� ������ �� ����� ���������� �� �������";
	string load_file_classrooms = "�������� ������ ��� ������ �� ����� ���������� �� ����������";
};

/// <summary>
/// ������� ��������� �� ������
/// </summary>
void cat_err() {
	cerr << "\n������������ ������" << endl;
	system("pause");
}


/// <summary>
/// ������� ��������� ��� ������������� ����������
/// </summary>
/// <param name="err_msg">error_msg - ��������� � ������� �� ������.</param>
void cat_exc(exception err_msg) {
	cerr << "\n�������� ���������� " << err_msg.what();
	system("pause");
}

//TODO : ��������, ��� ���� ���������� � ��� ����� �������

/// <summary>
/// �������� ���������� � ���� ������� ������
/// </summary>
/// <param name="filename">filename - ��� �����.</param>
void App_And_Save(char filename[]) {
	Classes a_class;
	string str;
	char choose = '\0';
	try {
		ofstream fileWithData(filename, ios::app);
		if (!fileWithData) {
			cout << "\n��������� ������ �������� ����� ��� ��������!" << endl
				<< "1. ��������� �����" << endl
				<< "2. ����� �� ���������" << endl
				<< "�����: ";
			cin >> choose;
			if (choose == '1')
				Input_Classes();
			if (choose == '2')
				exit(-1);
		}
		do {
			cout << "\n������� ����� ������: ";
			getline(cin, str);
			getline(cin, a_class.group);
			fileWithData << a_class.group << '\n';
			cout << "\n������� �������� ����������: ";
			getline(cin, a_class.discipline);
			fileWithData << a_class.discipline << '\n';
			cout << "\n������� �.�.�. �������������: ";
			getline(cin, a_class.teacher);
			fileWithData << a_class.teacher << '\n';
			cout << "\n������ ���������� ����? (y/n) ";
			cin >> choose;
		} while (choose == 'Y' || choose == 'y');
		fileWithData.close();
		Input_Classes();
	}
	catch (exception & error_message) {
		cat_exc(error_message);
		Input_Classes();
	}
	catch (...) {
		cat_err();
		Input_Classes();
	}
}

/// <summary>
/// ������ ������� ������ � ������ ��������� ����
/// </summary>
/// <param name="filename">filename - ��� �����.</param>
void Out_And_Save(char filename[]) {
	Classes a_class;
	string str;
	char choose = '\0';
	try {
		ofstream fileWithData(filename);
		do {
			cout << "\n������� ����� ������: ";
			getline(cin, str);
			getline(cin, a_class.group);
			fileWithData << a_class.group << '\n';
			cout << "\n������� �������� ����������: ";
			getline(cin, a_class.discipline);
			fileWithData << a_class.discipline << '\n';
			cout << "\n������� �.�.�. �������������: ";
			getline(cin, a_class.teacher);
			fileWithData << a_class.teacher << '\n';
			cout << "\n������ ���������� ����? (y/n) ";
			cin >> choose;
		} while (choose == 'Y' || choose == 'y');
		fileWithData.close();
		Input_Classes();
	}
	catch (exception & error_message) {
		cat_exc(error_message);
		Input_Classes();
	}
	catch (...) {
		cat_err();
		Input_Classes();
	}
}

/// <summary>
/// ���� �������� ����� ������� ������, ����� �������� ��� �������� ������ �����
/// </summary>
void Input_Classes() {
	system("cls"); 
	char filename[256], mode = '\0';
	cout << "���� ������� ������" << endl
		<< "\n1. ������� ����� ����" << endl
		<< "2. �������� � ������������" << endl
		<< "3. �����" << endl
		<< "4. ����� �� ���������" << endl
		<< "�����: ";
	try {
		cin >> mode;
		if (mode == '3')
			Main_Menu();
		else
			if (mode == '4')
				exit(0);
		cout << "\n������� ��� ����� ���������� � ��������: ";
		cin >> filename;
		if (mode == '1')
			Out_And_Save(filename);
		else
			if (mode == '2')
				App_And_Save(filename);
		Main_Menu();
	}
	catch (exception & err_message) {
		cat_exc(err_message);
		Main_Menu();
	}
	catch (...) {
		cat_err();
		Main_Menu();
	}
	Main_Menu();
}

/// <summary>
/// ���� ���������� �� ���������� � ����� ����
/// </summary>
/// <param name="filename">filename - ��� �����.</param>
void ClRooms_In(char filename[]) {
	Classrooms a_classroom;
	char choose = '\0';
	string str;
	try {
		ofstream fileWithData(filename);
		do {
			cout << "\n������� ����� ���������: ";
			getline(cin, str);
			getline(cin, a_classroom.number);
			fileWithData << a_classroom.number << '\n';
			cout << "\n������� ��� ���������: ";
			getline(cin, a_classroom.name);
			fileWithData << a_classroom.name << '\n';
			cout << "\n������ ���������� ����? (y/n) ";
			cin >> choose;
		} while (choose == 'Y' || choose == 'y');
		fileWithData.close();
		Input_Classrooms();
	}
	catch (exception & error_message) {
		cat_exc(error_message);
		Input_Classrooms();
	}
	catch (...) {
		cat_err();
		Input_Classrooms();
	}
}

/// <summary>
/// �������� ���������� �� ���������� � ��� ������������ ����
/// </summary>
/// <param name="filename">filename - ��� �����.</param>
void ClRooms_App(char filename[]) {
	Classrooms a_classroom;
	string str;
	char choose = '\0';
	try {
		ofstream fileWithData(filename, ios::app);
		if (!fileWithData) {
			cout << "\n��������� ������ �������� ����� ��� ��������!" << endl
				<< "1. ��������� �����" << endl
				<< "2. ����� �� ���������" << endl
				<< "�����: ";
			cin >> choose;
			if (choose == '1')
				Input_Classes();
			if (choose == '2')
				exit(-1);
		}
		do {
			cout << "\n������� ����� ���������: ";
			getline(cin, str);
			getline(cin, a_classroom.number);
			fileWithData << a_classroom.number << '\n';
			cout << "\n������� ��� ���������: ";
			getline(cin, a_classroom.name);
			fileWithData << a_classroom.name << '\n';
			cout << "\n������ ���������� ����? (y/n) ";
			cin >> choose;
		} while (choose == 'Y' || choose == 'y');
		fileWithData.close();
		Input_Classrooms();
	}
	catch (exception & error_message) {
		cat_exc(error_message);
		Input_Classrooms();
	}
	catch (...) {
		cat_err();
		Input_Classrooms();
	}
}

/// <summary>
/// ���� �������� ����� ���������, ����� �������� ��� �������� ������ �����
/// </summary>
void Input_Classrooms() {
	system("cls");
	char filename[256], mode = '\0';
	cout << "���� ������ �� ����������" << endl
		<< "\n1. ������� ����� ����" << endl
		<< "2. �������� � ������������" << endl
		<< "3. �����" << endl
		<< "4. ����� �� ���������" << endl
		<< "�����: ";
	try {
		cin >> mode;
		if (mode == '3')
			Main_Menu();
		else
			if (mode == '4')
				exit(0);
		cout << "\n������� ��� ����� ���������� �� ����������: ";
		cin >> filename;
		if (mode == '1')
			ClRooms_In(filename);	
		else
			if (mode == '2')
				ClRooms_App(filename);
		Main_Menu();
	}
	catch (exception & err_message) {
		cat_exc(err_message);
		Main_Menu();
	}
	catch (...) {
		cat_err();
		Main_Menu();
	}
	Main_Menu();
}

/// <summary>
/// ����� ���������� � ���������
/// </summary>
void FAQ_Output() {
	system("cls");
	cout << "				� ���������\n��������� ������������� ��� ����������� ���������� � ��������������\n"
		<< "������������� ���������\n�������� ""���������� ����������"", ����� ����������� ��������\n������� ������"
		<< " � ����������� ������������ ����������\n����� ������ ������ � ������� �����, �������� ""���� ������� ������"""
		<< endl;
	system("pause");
	Main_Menu();
}

/// <summary>
/// ������ �� ����� ������� ������
/// </summary>
/// <returns>std.vector&lt;_Ty, _Alloc&gt; - ������ ���������� ������� �� ������ ������.</returns>
vector <Classes> Load_Classes() {
	system("cls");
	Classes a_class;
	vector <Classes> classes_list;
	char filename[256], symbol = '\0';
	try {
		cout << "\n������� ��� �����: ";
		cin >> filename;
		if (!cin)
			throw inp_err;
		ifstream fileWithData(filename);
		while (!EOF) {
			fileWithData >> a_class.group;
			cout << a_class.group << " ";
			fileWithData >> a_class.discipline;
			cout << a_class.group << " ";
			fileWithData >> a_class.teacher;
			cout << a_class.group << endl;
			classes_list.push_back(a_class);
		}
		//������� �����
//		while ((symbol = classes_info.get()) != EOF) {
//			if (symbol == line_ends)
//				count_n++;
//		}
//		classes_info.clear();
//		classes_info.seekg(0);
		//������ �� ����� � ������ � ������
//		char remembering_string[N];
/*		for (int index_string = 1; index_string < count_n / num_of_options; index_string++) {
			classes_info.getline(remembering_string, N);
			a_class.group = unsigned int(remembering_string);
			classes_info.getline(remembering_string, N);
			strcpy_s(a_class.discipline, remembering_string);
			classes_info.getline(remembering_string, N);
			strcpy_s(a_class.teacher, remembering_string);
			classes_list.push_back(a_class);
		}
		classes_info.close();
*/
		fileWithData.close();
		return classes_list;
	}
	catch (int err_num) {
		cerr << "\n��������� ������ �����" << endl;
		exit(-1);
	}
	catch (exception & err_message) {
		cat_exc(err_message);
	}
	catch (...) {
		cat_err();
	}
}

/// <summary>
/// �������� � ����� ����������, ��� ������ � ����
/// </summary>
void Genetic_Algorithm() {//(vector <Classes> classes_list) {
	system("cls");
//	unsigned int num_of_stud_days = classes_list.size() / 4; //����� ���������� ����, ������� ������ ��� ������
	Classes a_class;
	vector <unsigned int> groups_list;
	cout << "		���� ������: �����������" << endl
		<< "������: �-303" << endl
		<< "1. ���������� ���������������� (������� �.�.), �� 107" << endl
		<< "2. ���������� ���������� � ������ ��� ������ (��������� �.�.), �� 105" << endl
		<< "3. ��������� ������ � ���������������� (�������� �.�.), ���. 45" << endl
		<< "4. ����������� (������ �.�.), �����. ���" << endl << endl
		<< "������: �-304�" << endl
		<< "1. ���������� ���������� � ������ ��� ������ (��������� �.�.), �� 105" << endl
		<< "2. ���������� ���������������� (������� �.�.), �� 107" << endl
		<< "3. ����������� (������ �.�.), �����. ���" << endl
		<< "4. ��������� ������ � ���������������� (�������� �.�.), ���. 45" << endl << endl;
	system("pause");
	//�������� ������������� ������ � ��������� �� ����������� ������� �����
//	for (int index = 0; index < classes_list.size() - 1; index++) {
//		if (classes_list[index].group > classes_list[index + 1].group) {
//			swap(classes_list[index], classes_list[index + 1]);
//		}
//	}
	//��� ������� ������ �� ������� ��������� �����
//	for (int index = 0; index < classes_list.size(); index++) {
//		if (index == 0)
//			groups_list.push_back(classes_list[index].group);
//		else {
//			if (classes_list[index].group != classes_list[index - 1].group)
//				groups_list.push_back(classes_list[index].group);
//		}
//		cout << classes_list[index].group << " ";
//	}
//	cout << "����� " << groups_list.size() << " �����" << endl;
}
	//��� - ����� ����� �������

/// <summary>
/// ������� ������� ���� ��� �����/������ ����������
/// </summary>
void Main_Menu() {
	system("cls");
	char choose = '\0';
	cout << "����� ���������� � ��������� ����������� �������� ����������!" << "\n\n"
		<< "�������� ���� �� ��������� ������� ����: " << endl
		<< "1. ���������� ����������" << endl
		<< "2. � ���������" << endl
		<< "3. ���� ������� ������" << endl
		<< "4. ���� ������ �� ����������" << endl
		<< "5. ����� �� ���������" << endl
		<< "�����: ";
	try {
		cin >> choose;
		if (!cin)
			throw inp_err;
	}
	catch (int err_num) {
		cerr << "\n�������� ������ �����!" << endl;
		exit(-1);
	}
	if (choose == '1') {
	//	vector <Classes> classes_list = Load_Classes();
		Genetic_Algorithm(); // (classes_list);
	}
	if (choose == '2') {
		FAQ_Output();
	}
	if (choose == '3') {
		Input_Classes();
	}
	if (choose == '4') {
		Input_Classrooms();
	}
	if (choose == '5') {
		system("cls");
		cout << "���������� ���������..." << endl;
		system("pause");
		exit(0);
	}
}

/// <summary>
/// ������� ������� ���������
/// </summary>
/// <returns>int - ��������� ���������� ���������.</returns>
int main()
{
	setlocale(LC_ALL, "RUS");
	Main_Menu();
	system("pause");
	return 0;
}